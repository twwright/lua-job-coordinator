
return {
    {
        baseOpt = {
            field = 10
        }
    },
    {
        name = 'Test',
        file = 'lua-job-coordinator/jobTest.lua',
        repeats = 5,
        opt = {
            field = 15
        },
        searchOpt = {
            learningRate = {
                type = "exponential",
                values = { -3, -6 }
            },
            pick = {
                type = "pick",
                values = {"opt1", "opt2", "opt3"}
            },
            weightDecay = {
                type = "uniform",
                values = { 0.01, 0.05 }
            }
        }
    }
}