-- Set up the command line params
cmd = torch.CmdLine()
cmd:text()
cmd:text('Running Jobs ')
cmd:text()
cmd:text('Options')
cmd:option('-jobs', 'jobs.lua', 'job file')
cmd:option('-dir', '', 'directory to run jobs in')
cmd:text()

-- work out our current path so we can execute jobRunner later
local scriptPath = debug.getinfo(1).short_src
local runnerPath = scriptPath:match("(.*/)") or ''
print('Executing jobRunner from: ' .. runnerPath .. 'jobRunner.lua')

-- parse arguments and attempt to load job file
params = cmd:parse(arg)
jobs = dofile(params.jobs)

-- Display list of jobs to run
print('Jobs loaded from: ' .. params.jobs)
print('--------')
print('JOB LIST')
print('--------')
for i, j in ipairs(jobs) do
    if j.base_opt or j.baseOpt then
        print(string.format('[%d] Base Opts', i))
    else
        print(string.format('[%d] %s (%s)', i, j.name, j.file))
    end
end

-- Define the function to run a job
function run_job(index, job)
  -- construct the command
  local job_file = params.jobs
  local cmd = string.format('th %s %s %s %s',
        runnerPath .. 'jobRunner.lua',
        '-jobs ' .. job_file,
        params.dir ~= '' and '-dir ' .. params.dir or '',
        '-job ' .. index)
  print(string.format('[%d] Executing: %s', index, cmd))
  -- execute
  os.execute(cmd)
end

-- Run jobs
print('------------')
print('RUNNING JOBS')
print('------------')

for i, j in ipairs(jobs) do
  if j.base_opt or j.baseOpt then
      -- do nothing, just skip
      print(string.format('[%d] Skipping base_opt...', i))
  else
      print(string.format('[%d] Starting job: %s (%s)', i, j.name, j.file))
      -- execute job runner for this job
      local status, err = xpcall(function() run_job(i, j) end, debug.traceback)
      if not status then
         print('Error while executing job runner! (' .. err .. ')')
      end
      print(string.format('[%d] Job finished! %s (%s)', i, j.name, j.file))
  end
end
