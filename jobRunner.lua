-- Set up the command line params
cmd = torch.CmdLine()
cmd:text()
cmd:text('Job Runner - runs a single job specified from a job file')
cmd:text()
cmd:text('Options')
cmd:option('-jobs', 'jobs.lua', 'job file')
cmd:option('-dir', '', 'directory to run job in')
cmd:option('-job', 'job index to run from the job file')
cmd:text()

-- parse arguments and attempt to load job file
local params = cmd:parse(arg)

-- load jobs and select job
local jobs = dofile(params.jobs)
local job_num = tonumber(params.job)
local job = jobs[job_num]

-- Define the function to run a job
function run_job(job)
  -- load base opts
  opt = {}
  for i = 1, job_num do
    if jobs[i].base_opt or jobs[i].baseOpt then
        log('Base parameters loaded from index ' .. i)
        for k, v in pairs(jobs[i].base_opt or jobs[i].baseOpt) do
            opt[k] = v
        end
    end
  end

  -- add in job opts
  for k, v in pairs(job.opt) do
    opt[k] = v
  end

  -- add in search opts
  for k, v in pairs(job.searchOpt or job.search_opt or {}) do
    assert(type(v.values) == "table", "values must be a table!")
    local generatedVal
    if v.type == "uniform" then
        assert(type(v.values[1]) == "number" and type(v.values[2]) == "number", "values must be numbers")
        generatedVal = torch.uniform(v.values[1], v.values[2])
    elseif v.type == "pick" then
        generatedVal = v.values[torch.random(#v.values)]
    elseif v.type == "exponential" then
        assert(type(v.values[1]) == "number" and type(v.values[2]) == "number", "values must be numbers")
        generatedVal = torch.uniform(1, 10) * torch.pow(10, torch.uniform(v.values[1], v.values[2]))
    end
    opt[k] = generatedVal
  end

  -- run job
  log(string.format('Running %s (%s)', job.name, job.file))
  local time = os.time()
  dofile(job.file)
  local elapsed = os.time() - time
  local time_str = string.format("%ds", elapsed)
  if elapsed > 60 then time_str = string.format("%.2fm", elapsed / 60) end
  if elapsed > 3600 then time_str = string.format("%.2fh", elapsed / 3600) end
  log('Job completed in ' .. time_str)
end

-- Set up logging
local log_file
local log_job
function log(text)
  local text = os.date('[%Y-%m-%d %X ') .. log_job .. '] ' .. text
  print(text)
  if log_file then
    log_file:writeString(text .. '\n')
  end
end

-- create the job directory
local dir = os.date('%Y-%m-%d_%X_') .. job.name
jobDir = params.dir .. dir .. path.sep
job_dir = jobDir
path.mkdir(job_dir)

-- iterations
local repeats = job.repeats or 1
for run = 1, repeats do

    if repeats > 1 then
        jobDir = params.dir .. dir .. path.sep .. run .. path.sep
        job_dir = jobDir
        path.mkdir(jobDir)
    end
    print(string.format('Directory: %s', jobDir));
    
    -- open the log file
    log_file = torch.DiskFile(jobDir .. 'log', 'w')
    log_file:autoSpacing()
    log_job = job.name
    
    -- run the job!
    local status, err = xpcall(function() run_job(job) end, debug.traceback)
    if not status then
        log('Error while running job! (' .. err .. ')')
    end
    
    -- close the job
    log_file:writeString('End of log')
    log_file:close()

    -- GC
    collectgarbage()
    collectgarbage()
end
